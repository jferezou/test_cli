import pytest
from assertpy import assert_that
from unittest.mock import patch

from app.cli import search_woeid

pytestmark = [pytest.mark.unit]


class TestSearchWoeid:
    @patch("app.cli.call_get_url", return_value=[{"location_type": "City", "woeid": 13452}])
    def test_search_woeid(self, mock_call_get_url):
        woeid_list = search_woeid("Paris", False)
        mock_call_get_url.assert_called_once_with("https://www.metaweather.com/api/location/search/?query=Paris", False)
        assert_that(woeid_list).is_equal_to([13452])

    @patch("app.cli.call_get_url", return_value=[])
    def test_search_woeid_empty_response(self, mock_call_get_url):
        with pytest.raises(ValueError):
            search_woeid("Porto-Vecchio", False)
        mock_call_get_url.assert_called_once_with("https://www.metaweather.com/api/location/search/?query=Porto-Vecchio", False)

    @patch("app.cli.call_get_url", return_value=[{"location_type": "not a city", "woeid": 0}])
    def test_search_woeid_not_city(self, mock_call_get_url):
        woeid_list = search_woeid("London", True)
        mock_call_get_url.assert_called_once_with("https://www.metaweather.com/api/location/search/?query=London", True)
        assert_that(woeid_list).is_equal_to([])
