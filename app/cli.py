from datetime import datetime

import click
import pytz
import requests

BASE_URL = "https://www.metaweather.com"
RAINING_LIST = ["t", "hr", "lr", "s"]


def call_get_url(url, debug):
    """
    Call the specific url and return json response
    :param url:
    :param debug:
    :return:
    """
    if debug:
        click.echo(f"Call: {url}.")
    response = requests.get(url).json()
    if debug:
        click.echo(f"response: {response}.")
    return requests.get(url).json()


def search_woeid(city, debug):
    """
    Get woeid for corresponding cities
    :param city:
    :param debug:
    :return:
    """
    url = "".join([BASE_URL, "/api/location/search/?query={}"]).format(city)
    woeid_list = []

    response = call_get_url(url, debug)

    if not response:
        raise ValueError(f"{city} was not found")
    for found_city in response:
        # we get woeid_list only for cities
        if found_city.get("location_type") == "City":
            woeid_list.append(found_city.get("woeid"))

    if debug:
        click.echo(f"woeid list found: {woeid_list}.")
    return woeid_list


def get_weather_city(woeid, debug):
    """
    Get today's weather and parent for the woeid
    :param woeid:
    :param debug:
    :return:
    """
    url = "".join([BASE_URL, "/api/location/{}/"]).format(woeid)
    response = call_get_url(url, debug)

    # get current date depending on city timezone
    tz = pytz.timezone(response.get("timezone"))
    today = datetime.now(tz=tz).strftime("%Y-%m-%d")
    if debug:
        click.echo(f"Today date for woeid {woeid}: {today}.")

    # try to find today's weather
    today_weather = next(
        (
            obj
            for obj in response.get("consolidated_weather")
            if obj.get("applicable_date") == today
        ),
        None
    )
    if not today_weather:
        raise ValueError(f"No weather found for {woeid} for the {today}")
    parent = response.get("parent").get("title")

    return parent, today_weather.get("weather_state_abbr")


@click.command()
@click.argument("city", nargs=1)
@click.option("--debug", "-d", help="enable debug logging")
def will_it_rain_today_in(city, debug=False):
    """
    Says it it will rain in an existing city (London, Paris ...)
    """
    cities_woeid = search_woeid(city, debug)
    for woeid in cities_woeid:
        parent, weather_state_abbr = get_weather_city(woeid, debug)

        if debug:
            click.echo(f"Weather for {city} - {parent}: {weather_state_abbr}.")

        if weather_state_abbr in RAINING_LIST:
            click.echo(f"It will rain today in {city} - {parent}")
        else:
            click.echo(f"It will not rain today in {city} - {parent}")
