from setuptools import setup

setup(
    name="weather",
    version='0.1',
    py_modules=['app.cli'],
    install_requires=[
        'Click',
        'requests',
        'pytz',
    ],
    entry_points='''
        [console_scripts]
        will_it_rain_today_in=app.cli:will_it_rain_today_in
    ''',
)
