# How it works

Get the woeid of corresponding cities using :\
/api/location/search/?query=(query)

Then use this woed to get the 5 days forecast using :\
/api/location/(woeid)/\
and extract the today's forcast prevision for each found cities.\
If the prevision indicates rain (showers, light rain, heavy rain or thunderstorm) return true, else false

I dit not use the /api/location/(woeid)/(date)/ api, because of the hight number of prevision (which one to take ? first one ?) and the abscence of detailled city information (country ..)

# Installation :

Clone project : git clone https://gitlab.com/jferezou/test_cli.git\
cd test_cli \
pipenv shell \
pip install --editable .

# Use command line
###  run command line :
will_it_rain_today_in {city name}

### output :
List of corresponding cities and if it will rain today

### example :
will_it_rain_today_in "san diego" \
It will not rain today in san diego - California

will_it_rain_today_in paris \
It will not rain today in paris - France

# Improvements :
-   Finish unittest
-   Better errors management
